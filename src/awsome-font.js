/**
 * Main file
 *
 * @author Zaid Hoona
 */

var cFonts = require("cfonts");
var program = require("commander");

function list(val) {
  return val.split(",");
}

program
  .version("1.0")
  .usage("[options] <text>")
  .option(
    "-c, --color [colors...]",
    "color , default color is white",
    list,
    "white"    
  )
  .option(
    "-f, --font [console|block|simple|simpleBlock|3d|chrome]",
    "font to display on the console",
    /^(console|block|simple|simpleBlock|3d|chrome)$/i,
    "console"
  )
  .option(
    "-a, --align [left|right|center]",
    "align text left|right|center",
    /^(left|right|center)$/i,
    "left"
  )
  .parse(process.argv);

var text;

if (!program.args) {
  console.error("Error: Please provide the text to display");
  process.exit(1);
} else if (program.args.length === 1) {
  text = program.args[0];
} else {
  program.args.forEach(function(i) {
    text = text ? text.concat("|").concat(i) : i;
  });
}

console.log(
  cFonts.render(text, {
    align: program["align"],
    font: program["font"],
    colors: program["color"]
  }).string
);
